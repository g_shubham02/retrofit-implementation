package com.example.api

import android.app.Application
import com.example.api.api.RetrofitHelper
import com.example.api.api.UsersService
import com.example.api.repository.UserRepository

class UserApplication : Application(){
    lateinit var userRepository: UserRepository

    override fun onCreate() {
        super.onCreate()
        initialize()
    }

    private fun initialize() {
        val usersService = RetrofitHelper.getInstance().create(UsersService::class.java)
        userRepository = UserRepository(usersService)
    }
}