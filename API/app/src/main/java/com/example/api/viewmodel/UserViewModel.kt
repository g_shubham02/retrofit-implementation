package com.example.api.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.api.repository.UserRepository
import androidx.lifecycle.viewModelScope
import com.example.api.data.Data
import com.example.api.data.UserModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call

class UserViewModel(private val repository: UserRepository) : ViewModel(){
    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getUsers(2)
        }
    }

    val users : LiveData<UserModel>
        get() = repository.users

    fun createUser(user:Data) {
        repository.createUser(user)
    }
}