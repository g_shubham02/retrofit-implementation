package com.example.api

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.api.api.RetrofitHelper
import com.example.api.api.UsersService
import com.example.api.repository.UserRepository
import com.example.api.viewmodel.UserViewModel
import com.example.api.viewmodel.UserViewModelFactory

class UserListActivity : AppCompatActivity() {
    val tag = "UserListActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)

        getUsers()
    }

    inner class DownloadImageFromInternet(
        var imageView: ImageView,
        email1: String,
        firstN: String,
        lastN: String
    ) :
        AsyncTask<String, Void, Bitmap?>() {
        init {
            Toast.makeText(
                applicationContext,
                "Please wait, it may take a few minute...",
                Toast.LENGTH_SHORT
            ).show()

            val email = findViewById<TextView>(R.id.emailText)
            val firstname = findViewById<TextView>(R.id.first_name)
            val lastName = findViewById<TextView>(R.id.last_name)
            email.text = email1
            firstname.text = firstN
            lastName.text = lastN
        }

        override fun doInBackground(vararg urls: String?): Bitmap? {
            val imageURL = urls[0]
            var image: Bitmap? = null
            try {
                val `in` = java.net.URL(imageURL).openStream()
                image = BitmapFactory.decodeStream(`in`)
            } catch (e: Exception) {
                Log.e("Error Message", e.message.toString())
                e.printStackTrace()
            }
            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            imageView.setImageBitmap(result)
        }
    }

    private fun getUsers() {
        val repository = (application as UserApplication).userRepository
        val viewModel= ViewModelProvider(this, UserViewModelFactory(repository))[UserViewModel::class.java]

        viewModel.users.observe(this, Observer {
            val users=it
            val imageURL = users?.data?.get(0)?.avatar
            val email = users?.data?.get(0)?.email.toString()
            val firstname = users?.data?.get(0)?.first_name.toString()
            val lastName = users?.data?.get(0)?.last_name.toString()

            DownloadImageFromInternet(
                findViewById(R.id.userImage),
                email,
                firstname,
                lastName
            ).execute("$imageURL")
            Log.d(tag, users?.data.toString())
        })
    }
}