package com.example.api

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.ViewModelProvider
import com.example.api.data.Data
import com.example.api.viewmodel.UserViewModel
import com.example.api.viewmodel.UserViewModelFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    val tag = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val login = findViewById<Button>(R.id.login)
        val register = findViewById<Button>(R.id.register)
        val usersList = findViewById<Button>(R.id.usersList)

        login.setOnClickListener { loginUser() }
        register.setOnClickListener { registerUser() }
        usersList.setOnClickListener { showUsers() }
    }

    private fun loginUser() {

    }

    private fun registerUser() {
        val repository=(application as UserApplication).userRepository
        val viewModel= ViewModelProvider(this, UserViewModelFactory(repository))[UserViewModel::class.java]

        viewModel.createUser(
            Data(
                "fer",
                "email.text.toString()",
                "firstName.text.toString()",
                0,
                "fer"
            )
        )

    }

    private fun showUsers() {
        val intent = Intent(this, UserListActivity::class.java)
        startActivity(intent)
    }
}