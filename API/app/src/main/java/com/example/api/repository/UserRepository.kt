package com.example.api.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.api.data.UserModel
import com.example.api.api.UsersService
import com.example.api.data.Data
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository(private val userService: UsersService) {
    private val tag = "UserRepository"
    private val usersLiveData = MutableLiveData<UserModel>()

    val users: LiveData<UserModel>
        get() = usersLiveData

    suspend fun getUsers(page: Int) {
        val result = userService.getUsers(page)
        if (result.body() != null) {
            usersLiveData.postValue(result.body())
        }
    }

    fun createUser(user: Data) {
        val call = userService.createUser(user)
        call.enqueue(object : Callback<Data> {
            override fun onResponse(call: Call<Data>, response: Response<Data>) {
                Log.d(tag, response.code().toString())
            }

            override fun onFailure(call: Call<Data>, t: Throwable) {
                Log.d(tag, "No user created")
            }

        })
    }
}