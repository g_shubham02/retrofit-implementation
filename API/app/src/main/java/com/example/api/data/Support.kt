package com.example.api.data

data class Support(
    val text: String,
    val url: String
)