package com.example.api.api

import com.example.api.data.Data
import com.example.api.data.UserModel
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface UsersService {
    @GET("api/users?")
    suspend fun getUsers(@Query("page") page: Int): Response<UserModel>

    @POST("api/users")
    fun createUser(@Body user: Data): Call<Data>
}

